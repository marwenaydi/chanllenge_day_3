from django.shortcuts import render
import random
import uuid
import datetime
from datetime import date,time
from django.http import JsonResponse, response
from django.core import serializers
import string
import urllib, json
from django.core.mail import send_mail 
from django.conf import settings
from django.shortcuts import render
import operator
from .models import *
from django.http import HttpResponse
import sys


# Create your views here.
erreur=list()
def get_reservations(request):
    rooms = {
    "roles":{
        "type":"list",
        "required":True,
        "validation":[{
            "field":"room_id_pms", "roles":{"type":"str", "required":True}
        },
            {
                "field":"room_type_pms", "roles":{"type":"str", "required":True}
            }]
    }
}
    validateur = [
    {"field":"first_name", "roles": {
        "type": "str",
        "required":True
    }},
    {"field":"last_name", "roles": {
        "type": "str",
        "required":True
    }},
    {"field":"reservation_id_pms", "roles": {
        "type": "str",
        "required":True
    }},
    {
        "field":"rooms", **rooms
    },
    {
        "field":"invoice", "roles": {"type":"dict", "required":False},"convert":True, "validation":[{
        "field":"restervation_additional_products", "roles":{
            "required":False, "type":"list", "validation":[
                {"field":"product_name", "roles":{'type':"str", "required":True}},
                {"field": "product_quantity", "roles": {'type': "int", "required": True}},
                {"field": "product_id", "roles": {'type': "str", "required": True}},
                {"field": "product_unite_price", "roles": {'type': "int", "required": True}},
            ]
        }
    }]
    }
]
    reservation={
  "customer": 52,
  "reservation_id_pms": "033351",
  "reservation_code": "033351",
  "reservation_channel_number": "None",
  "reservation_group_id_pms": "None",
  "arrival_time_estimated": "16:00:00",
  "departed_time_estimated": "12:00:00",
  "is_main_reservation": True,
  "start_date": "2021-10-08",
  "end_date": "2021-10-09",
  "number_of_adults": 2,
  "number_of_keys": 2,
  "number_of_children": 0,
  "number_of_guests": 2,
  "number_of_night": 1,
  "status": "Confirmed",
  "checked_in": False,
  "checked_out": False,
  "guests": [
    {
      "guest_id_pms": "111410",
      "title": "",
      "first_name": "Jens Erik",
      "last_name": "Hilsen",
      "email": "jens-erik.hilsen@telenor.no",
      "phone": "",
      "address1": "",
      "address2": "",
      "gender": 1,
      "city": "",
      "passport_number": "",
      "state": "",
      "country": "NO",
      "nationality": "NO",
      "zip_code": "",
      "reservation_guest_data": {
        "start_date": "2021-10-08",
        "end_date": "2021-10-09"
      },
      "reservation_id_pms": "033351",
      "is_main_guest": True,
      "general_data": "{\"address1Field\": \"\", \"address2Field\": \"\", \"address3Field\": \"\", \"agencyField\": false, \"agencyFieldSpecified\": true, \"cityField\": \"\", \"commentField\": \"\", \"companyNameField\": \"Hilsen, Jens Erik\", \"contactNumberField\": \"111410\", \"countryField\": {\"code2DigitField\": \"NO\", \"code3DigitField\": \"NOR\", \"codeHSField\": null, \"countryNameField\": \"Norway\"}, \"dateOfBirthField\": \"/Date(-62135596800000)/\", \"dateOfBirthFieldSpecified\": true, \"emailField\": \"jens-erik.hilsen@telenor.no\", \"faxField\": null, \"firstNameField\": \"Jens Erik\", \"genderField\": 1, \"genderFieldSpecified\": true, \"isVipField\": false, \"isVipFieldSpecified\": true, \"lastNameField\": \"Hilsen\", \"nameField\": \"Hilsen, Jens Erik\", \"nationalityField\": {\"code2DigitField\": \"NO\", \"code3DigitField\": \"NOR\", \"codeHSField\": null, \"countryNameField\": \"Norway\"}, \"passportNumberField\": null, \"permanentField\": false, \"permanentFieldSpecified\": true, \"socialSecurityNumberField\": null, \"stateField\": \"\", \"telephoneField\": null, \"titleField\": null, \"typeField\": 0, \"typeFieldSpecified\": true, \"userDefined1Field\": \"\", \"userDefined2Field\": \"\", \"userDefined3Field\": \"\", \"userDefined4Field\": \"\", \"userDefined5Field\": \"\", \"vIPGroupField\": \"ToDo Contact\", \"zIPField\": \"\"}"
    }
    
  ],
  "country_main_guest": "NO",
  "nationality_main_guest": "NO",
  "gender_main_guest": "",
  "first_name_main_guest": "Jens Erik",
  "last_names_main_guest": "Hilsen",
  "email_main_guest": "jens-erik.hilsen@telenor.no",
  "title_main_guest": "",
  "main_guest_id": "111410",
  "language_main_guets": "",
  "rooms": [
    {
      "room_id_pms": 85,
      "room_name": "VILLA/407  ",
      "room_number": "000407",
      "room_number_to_display": "407",
      "room_type_pms": "SUI  ",
      "room_type": "sui  ",
      "room_floor": "VILLA",
      "room_category_name": "Suite",
      "reservation_room_data": {
        "start_date": "2021-10-08",
        "end_date": "2021-10-09",
        "guests": [
          "111410"
        ],
        "number_of_keys": 2,
        "guest_id_pms": "111410",
        "guest_first_name": "Jens Erik",
        "guest_last_name": "Hilsen",
        "number_of_children": 0,
        "rates": [
          
        ],
        "balance": 0,
        "is_available": True,
        "reservation_id_pms": "033351",
        "number_of_adults": 2,
        "room_total": 0
      }
    }
  ],
  "number_of_rooms": 1,
  "main_room_number": "407",
  "total": 0,
  "balance": 0,
  "paid": 0,
  "invoice": "{\"reservation_additional_products\": [{\"product_name\": \"Night 08/10/2021\", \"product_net\": -13.0, \"product_quantity\": 1, \"product_total\": 0.0, \"product_unite_price\": 0.0, \"product_id\": \"\", \"product_tax\": 13.0, \"product_type\": \"Room\", \"product_tax_rate\": -100.0, \"consumption_date\": \"2021-10-08\", \"product_note\": \"\"}], \"invoice_detailed\": {\"balance\": 0.0, \"total\": 0.0, \"invoice_items_quantity\": 1, \"currency\": \"Nok\", \"paid\": 0.0}}"
} 
    validation(reservation,validateur)
    parcour(reservation,validateur)
    errListe = list(set(erreur))
    new_list=dels(errListe)
    return JsonResponse(new_list,safe=False)

def parcour(reservation,validateur):
      if isinstance(reservation, dict):
        for k in reservation.keys():
          if(type(reservation[k]) != str and type(reservation[k]) != int and type(reservation[k]) != bool and type(reservation[k]) != type(None)):
            if(type(reservation[k]) == list):
              for v in (reservation[k]):
                validation(v,validateur)
                parcour(v,validateur)
            else:
                validation(reservation[k],validateur)
                parcour(reservation[k],validateur)
          else:
            print(k)
      else:
        print(reservation)
def validation(reservation,validateur):
    for idx, AttrVal in enumerate(validateur):
      if (AttrVal['field'] in reservation):
        erreur.append(AttrVal['field'])
        req=len(reservation[AttrVal['field']])
        if(bool(req)==AttrVal['roles']['required']):
          erreur.append('required '+AttrVal['field'])
        else:
          erreur.append('erreur required '+AttrVal['field'])
        if(type(reservation[AttrVal['field']]))==(((eval(AttrVal['roles']['type'])))):
           erreur.append('type '+AttrVal['field'])
        else:
           erreur.append('erreur type '+AttrVal['field'])
      else:
        erreur.append('erreur '+AttrVal['field'])
      if('validation' in AttrVal):
        validation(reservation,AttrVal['validation'])
def dels (errListe):
  i=0
  new_list=list()
  for attr in errListe:
    if (attr.find("erreur")!=-1):
     for i in range(0,len(errListe)):
       if('erreur '+errListe[i])==attr:
         new_list.append(errListe[i])
         new_list.append(attr)
    else:
      new_list.append(attr)
  for element in new_list:
    if element in errListe:
        errListe.remove(element) 
  return errListe



  
    


       











       

    
    

  
